## Actor model
The actor model - is the *conceptual* model of concurrent computation originated in 1973. For now, most famous implementation of actor model conception its *Akka* toolkit and *Elixir* programming language.

## Basics about the actor model

### Actor
In the actor model, **actor** - is the fundamental unit of compitation. 
Actor can hold own *private state*, and can use it to deside how to handle the next message based on previous state. 
Actors *are lightweight*. It's easy to create thousands or even millions of them, as they require fewer resources than threads.
Actors are *isolated* from each over and do not share memory. The only way to change actor state it's by receiving the messages. Actors are decoupled and they work asynchronously. They don't need to wait for a response from another actor. As well actors have addresses. 
Actor can run locally or remotely on another machine.

#### Actor allowed operations:
* create another actor;
* send a message;
* designate how to handle the next message.


### Actor addresses
Actor can communicate with actor whose addresses it has. 
Actor knowing addresses:
* current knowing actor addresses;
* addresses of another actor it ever communicate;
* actors addresses it has itself created (child actors);
* obtained addresses from the messages it receives.

One actor can have many addresses. Address is not equal to identity.


### Actor mailboxes
Every actor has it's own *mailbox* which is similar to a message queue. Received messages are stored in actor mailbox until they are processed. Messages are sent to actors mailboxes and processed in FIFO order. In general conception it's not guarantee that messages should be delivered to the actor in the same order it's has been sent. It's depends on the implementation.


### Actor messages
After actors has been created they are waiting messages to arrive. Actors can communicate with each other only through messages.
Messages are simple immutable data structures that can be easily sent over the network. Conceptually an actor can handle only one message at a time. For sending message to another actor, producer actor should have address of it.


## Fault tolerance
In the actor model, actors can supervise other actors. Top level actor can decide what to do in case of failure. Superviser can restart supervised actor or redirect messages to another supervised actor. It's scheme can be represented as tree. For example:

                                           +-------------+
                                           |  Top level  |
                                           |  supervisor |
                                           +-------------+
                                           |             |
                                           |             |
                  +-------------+----------+             +----+-------------+
                  |  Supervisor |                             | Supervisor  |
                  +-------------+                             +-------------+
                  |             |                             |             |
                  |             |                             |             |
    +------------++           +-+----------+        +---------+--+          ++------------+
    |   Actor    |            |   Actor    |        |   Actor    |           |   Actor    |
    +------------+            +------------+        +------------+           +------------+


## Pros and Cons of the actor model

#### Pros:
1. Easy to scale;
2. Fault tolerance;
3. Geographical distribution;
4. No shared state.

#### Cons:
1. Suseptible to deadlocks;
2. Mailbox overflowing.
